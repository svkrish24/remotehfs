#!/usr/bin/env python

import logging, pymongo

from collections import defaultdict
from errno import ENOENT
from stat import S_IFDIR, S_IFLNK, S_IFREG
from sys import argv, exit
from time import time
from pymongo import MongoClient

from fuse import FUSE, FuseOSError, Operations, LoggingMixIn


if not hasattr(__builtins__, 'bytes'):
    bytes = str

proxy = xmlrpclib.ServerProxy("http://localhost:51234/") # Proxy is a server instance that connects to the Server simpleht.py 

def putData(key, value):		# client stores the data in server as a key value pair where key is the file node's path name and the value can be file/directory metadata or the file contents
	proxy.put(Binary(key), Binary(pickle.dumps(value)), 3000)
	return

def getData(key):		# client fetches the file node from server if its present
	return pickle.loads(proxy.get(Binary(key))['value'].data)

def removeData(key):		# client removes the file node from server
	proxy.put(Binary(key), Binary(pickle.dumps('')), 0)
	return

def movContent(dirNode, oldPath, newPath):  # This function is used to change the absolute path name of all child files and directories with which they are stored when the parent directory is moved or renamed
	for x in dirNode: 
		if x.find('/') == -1:
			childDir = getData(oldPath+'/'+x)
			putData(newPath+'/'+x, childDir)
			movContent(childDir, oldPath+'/'+x, newPath+'/'+x)
			removeData(oldPath+'/'+x)
		if x.count('/') == 2:
			childFile = getData(oldPath+'/'+x.split('/')[-1])
			putData(newPath+'/'+x.split('/')[-1], childFile)
			removeData(oldPath+'/'+x.split('/')[-1])
			dataRead = getData('Data-'+oldPath+'/'+x.split('/')[-1])
			putData('Data-'+newPath+'/'+x.split('/')[-1], dataRead)
			removeData('Data-'+oldPath+'/'+x.split('/')[-1])	
	
	return		

class Memory(LoggingMixIn, Operations):   #Hierarchical file system which stores its file contents and directories as individual file nodes in server

    def __init__(self):
        self.files = {}
        self.data = defaultdict(bytes)
        self.fd = 0
        now = time()
        self.files['/'] = dict(st_mode=(S_IFDIR | 0755), st_ctime=now, st_mtime=now, st_atime=now, st_nlink=2)
	putData('/', self.files)


    def chmod(self, path, mode):
	pathList = path.split('/')			
	fsNode = getData(path)
	if '/'+pathList[-1] in fsNode:
 		fsNode['/'+pathList[-1]]['st_mode'] &= 0770000
       		fsNode['/'+pathList[-1]]['st_mode'] |= mode
	else:
        	fsNode['st_mode'] &= 0770000
        	fsNode['st_mode'] |= mode
	putData(path, fsNode)		
	print 'chmod executed..'
        return 0

    def chown(self, path, uid, gid):
	pathList = path.split('/')			
	fsNode = getData(path)
	if '/'+pathList[-1] in fsNode:
		fsNode['/'+pathList[-1]]['st_uid'] = uid
        	fsNode['/'+pathList[-1]]['st_gid'] = gid
	else:
        	fsNode['st_uid'] = uid
        	fsNode['st_gid'] = gid		
	putData(path, fsNode)

    def create(self, path, mode):
	pathList = path.split('/')
	if '/'.join(pathList[:-1]) == '':
		pathCwd = '/'
	else:
		pathCwd = '/'.join(pathList[:-1])
	cwd = getData(pathCwd)
	print 'cwd is', cwd
	fileName = '/'+pathList[-2]+'/'+pathList[-1]
	cwd[fileName] = ''
	newFile = dict(st_mode=(S_IFREG | mode), st_nlink=1, st_size=0, st_ctime=time(), st_mtime=time(), st_atime=time())
	putData(pathCwd, cwd)
	putData(path, newFile)	
	print 'cwd now is', cwd
        self.fd += 1
	print 'RETURN VALUE', self.fd
	print 'create executed..'
        return self.fd

    def getattr(self, path, fh=None):
	print 'PATH is ', path
	if path == '/':
		cwd = getData('/')
		print 'cwd is', cwd
		print 'RETURN VALUE', cwd[path]
		print 'getattr executed..'
		return cwd[path]		
	pathList = path.split('/')
	fileName = '/'+pathList[-2]+'/'+pathList[-1]
	if '/'.join(pathList[:-1]) == '':
		pathCwd = '/'
	else:
		pathCwd = '/'.join(pathList[:-1])
	cwd = getData(pathCwd)
	if pathList[-1] not in cwd and fileName not in cwd:
		print 'This is d pbm'
                raise FuseOSError(ENOENT)
	fsNode = getData(path)
	print 'fsNode is', fsNode
	if fileName in cwd:
		print 'FileName', fileName
		print 'RETURN VALUE', fsNode
		print 'getattr executed..'
        	return fsNode
	print 'RETURN VALUE', fsNode['/'+pathList[-1]]
	print 'getattr executed..'
        return fsNode['/'+pathList[-1]]

    def getxattr(self, path, name, position=0):	
	pathList = path.split('/')
	fsNode = getData(path)
	if '/'+pathList[-1] in fsNode:
		attrs = fsNode['/'+pathList[-1]].get('attrs', {})
	else:		
        	attrs = fsNode.get('attrs', {})

        try:
	    print 'RETURN VALUE', attrs[name]
	    print 'Path is ', path
	    print 'getxattr executed..'
            return attrs[name]
        except KeyError:
	    print 'getxattr keyerror executed..'
            return ''       # Should return ENOATTR

    def listxattr(self, path):		
	pathList = path.split('/')			
	fsNode = getData(path)
	if '/'+pathList[-1] in fsNode:
		attrs = fsNode['/'+pathList[-1]].get('attrs', {})
	else:		
        	attrs = fsNode.get('attrs', {})
	print 'listxattr executed..'
        return attrs.keys()
			
    def mkdir(self, path, mode):
	pathList = path.split('/')
	if '/'.join(pathList[:-1]) == '':
		pathCwd = '/'
	else:
		pathCwd = '/'.join(pathList[:-1])
	cwd = getData(pathCwd)
	newDir = {}
	cwd[pathList[-1]] = {}
	newDir['/'+pathList[-1]] = dict(st_mode=(S_IFDIR | mode), st_nlink=2, st_size=0, st_ctime=time(), st_mtime=time(), st_atime=time())
	cwd['/'+pathList[-2]]['st_nlink'] += 1	
	putData(path, newDir)
	putData(pathCwd, cwd)
	print 'Path is', path 
	print 'mkdir executed..'

    def open(self, path, flags):		
        self.fd += 1
	print 'open executed..'
	print 'Return value', self.fd
        return self.fd

    def read(self, path, size, offset, fh):	
	print 'read executing..'
	dataRead = getData('Data-'+path)
	print 'path is', path
	print 'offset is', offset
	print 'size', size
	print 'dataRead is', dataRead	
	print 'Return value', dataRead[offset:offset + size]
        return str(dataRead[offset:offset + size])

    def readdir(self, path, fh):
	print 'readdir executing..'
	print 'Path is ', path
	pathList = path.split('/')
	fsNode = getData(path)
	print ['.', '..'] + [x.split('/')[-1] for x in fsNode if x != '/'+pathList[-1]]
	for x in fsNode:
		print x 
        return ['.', '..'] + [x.split('/')[-1] for x in fsNode if x != '/'+pathList[-1]]

    def readlink(self, path):			
	print 'readlink executed..'
	dataRead = getData('Data-'+path)
	print 'Return Value', dataRead
        return dataRead

    def removexattr(self, path, name):	
	pathList = path.split('/')
	fsNode = getData(path)
	if '/'+pathList[-1] in fsNode:
		attrs = fsNode['/'+pathList[-1]].get('attrs', {})
	else:		
        	attrs = fsNode.get('attrs', {})

        try:
	    print 'removexattr executing..'
            del attrs[name]
	    putData(path, fsNode)
        except KeyError:
	    print 'removexattr keyerror executed..'
            pass        # Should return ENOATTR

    def rename(self, old, new):	
        print 'rename executing..'
	print 'Old', old
	print 'New', new
	oldList = old.split('/')
	newList = new.split('/')
	if '/'.join(oldList[:-1]) == '':
		oldCwd = '/'
	else:
		oldCwd = '/'.join(oldList[:-1])
	if '/'.join(newList[:-1]) == '':
		newCwd = '/'
	else:
		newCwd = '/'.join(newList[:-1])
	cwdOld = getData(oldCwd)
	cwdNew = getData(newCwd)
	oldFileName = '/'+oldList[-2]+'/'+oldList[-1]
	newFileName = '/'+newList[-2]+'/'+newList[-1]
	if oldFileName in cwdOld:				#for moving files into directories or renaming files
		cwdNew[newFileName] = cwdOld.pop(oldFileName)
		if oldCwd == newCwd:
			cwdNew.pop(oldFileName)
		fileNode = getData(old)
		putData(new, fileNode)
		removeData(old)
		dataRead = getData('Data-'+old)
		putData('Data-'+new, dataRead)
		removeData('Data-'+old)		
	else:
		cwdNew[newList[-1]] = {}			#for moving directories into directory or renaming directory
		dirNode = getData(old)
		dirInfo = dirNode.pop('/'+oldList[-1])
		newDirNode = dirNode
		newDirNode['/'+newList[-1]] = dirInfo
		for x in newDirNode:
			if(x.count('/') == 2):
				newDirNode['/'+newList[-1]+'/'+x.split('/')[-1]] = newDirNode.pop(x)
		cwdOld['/'+oldList[-2]]['st_nlink'] -= 1
		cwdNew['/'+newList[-2]]['st_nlink'] += 1
		movContent(dirNode, old, new)		#calls movData for modifying the path name of the files and then restoring their data content accordingly 
		putData(new, newDirNode)
		removeData(old)
		print 'newDirNode is', newDirNode
		if oldCwd == newCwd:
			cwdNew.pop(oldList[-1])		
		cwdOld.pop(oldList[-1])
	print 'cwdOld is', cwdOld
	print 'cwdNew is', cwdNew		
	putData(oldCwd, cwdOld)
	putData(newCwd, cwdNew)	

    def rmdir(self, path):
	print 'rmdir executing..'
	print 'Path is', path
	pathList = path.split('/')
	if '/'.join(pathList[:-1]) == '':
		pathCwd = '/'
	else:
		pathCwd = '/'.join(pathList[:-1])
	cwd = getData(pathCwd)
        cwd.pop(pathList[-1])
 	cwd['/'+pathList[-2]]['st_nlink'] -= 1
	putData(pathCwd, cwd)
	removeData(path)

    def setxattr(self, path, name, value, options, position=0):	
        # Ignore options
	print 'setxattr executing..'
	pathList = path.split('/')
	fsNode = getData(path)
	if '/'+pathList[-1] in fsNode:
		attrs = fsNode['/'+pathList[-1]].setdefault('attrs', {})
	else:		
        	attrs = fsNode.setdefault('attrs', {}) 
        attrs[name] = value
	putData(path, fsNode)

    def statfs(self, path):		
	print 'statfs executed..'
        return dict(f_bsize=512, f_blocks=4096, f_bavail=2048)

    def symlink(self, target, source):	
	pathList = target.split('/')
	if '/'.join(pathList[:-1]) == '':
		pathCwd = '/'
	else:
		pathCwd = '/'.join(pathList[:-1])
	cwd = getData(pathCwd)		
	fileName = '/'+pathList[-2]+'/'+pathList[-1]
	cwd[fileName] = ''
	newFile = dict(st_mode=(S_IFLNK | 0777), st_nlink=1, st_size=len(source))
	putData(pathCwd, cwd)
	putData(target, newFile)	
	putData('Data-'+target, source)

    def truncate(self, path, length, fh=None):
	print 'truncate executing..'
	print 'Path is', path
	pathList = path.split('/')
	fileNode = getData(path)
	dataRead = getData('Data-'+path)
        dataRead = dataRead[:length]
        fileNode['st_size'] = length
	putData(path, fileNode)
	putData('Data-'+path, dataRead)

    def unlink(self, path):
	print 'unlink executing..'
	print 'Path is', path
	pathList = path.split('/')
	fileName = '/'+pathList[-2]+'/'+pathList[-1]
	if '/'.join(pathList[:-1]) == '':
		pathCwd = '/'
	else:
		pathCwd = '/'.join(pathList[:-1])
	cwd = getData(pathCwd)
        cwd.pop(fileName)
	putData(pathCwd, cwd)
	removeData(path)
	removeData('Data-'+path)

    def utimens(self, path, times=None):
	print 'Path is', path
	pathList = path.split('/')
	fileNode = getData(path)
        now = time()
        atime, mtime = times if times else (now, now)
	print 'utimens executing..'
        fileNode['st_atime'] = atime
        fileNode['st_mtime'] = mtime
	putData(path, fileNode)

    def write(self, path, data, offset, fh):
	pathList = path.split('/')
	fileNode = getData(path)
	dataRead = getData('Data-'+path)
	print 'path is', path
	print 'dataRead is', dataRead
	print 'dataRead[:offset] is', dataRead[:offset]
	print 'data is', data
        dataRead = dataRead[:offset] + data
	print 'dataRead now is', dataRead
        fileNode['st_size'] = len(dataRead)
	putData(path, fileNode)
	putData('Data-'+path, dataRead)	
	print 'RETURN VALUE', len(data)
	print 'write executed..'
        return len(data)


if __name__ == '__main__':
    if len(argv) != 2:
        print('usage: %s <mountpoint>' % argv[0])
        exit(1)

    logging.getLogger().setLevel(logging.DEBUG)
    fuse = FUSE(Memory(), argv[1], foreground=True, debug=True)
